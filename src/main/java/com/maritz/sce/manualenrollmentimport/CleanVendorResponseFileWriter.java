package com.maritz.sce.manualenrollmentimport;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public class CleanVendorResponseFileWriter {
    
    private static final String[] VENDOR_RESPONSE_COLUMN_ORDER = {
            "Vendor Number", "Vendor Account Group", "Tax Number 1", "Tax Number 2", "Vendor Name Line 1", "Vendor Name Line 2"
            , "House Number", "Street", "Supplemental", "City", "State/Region", "ZIP", "Email1", "Email2", "Email3", "Email4"
            , "CTRY", "Bank Key", "Bank Account", "Acct Holder", "Reference Details", "Name of Bank"};
    
    public void write(String filename, List<VendorResponseRecord> data) throws IOException, CsvRequiredFieldEmptyException, CsvDataTypeMismatchException {
        Writer writer = new FileWriter(filename);
        HeaderColumnNameMappingStrategy<VendorResponseRecord> mappingStrategy = new HeaderColumnNameMappingStrategy<>();
        mappingStrategy.setType(VendorResponseRecord.class);
        mappingStrategy.setColumnOrderOnWrite(new ColumnNameOrderComparator(VENDOR_RESPONSE_COLUMN_ORDER));
        
        StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer)
                .withMappingStrategy(mappingStrategy)
                .build();
        beanToCsv.write(data);
        writer.close();
         
    }

}
