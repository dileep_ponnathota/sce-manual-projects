package com.maritz.sce.manualenrollmentimport;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ColumnNameOrderComparator implements Comparator<String> {
    private List<String> columnNameOrder;

    public ColumnNameOrderComparator(String[] columnNameOrder) {
        this.columnNameOrder = new ArrayList<>();
        for (String item : columnNameOrder) {
            this.columnNameOrder.add(item.toLowerCase());
        }
    }

    @Override
    public int compare(String o1, String o2) {
        return columnNameOrder.indexOf(o1.toLowerCase()) - columnNameOrder.indexOf(o2.toLowerCase());
    }
}